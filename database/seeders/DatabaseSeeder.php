<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Create demo referrer
        $user = User::factory()->create([
            'name' => 'Test User',
            'email' => 'demo@livefront.com'
        ]);
        // Create referees
        User::factory()->count(3)->referred_by($user->referral_code)->state(new Sequence(
            ['email_verified_at' => now()],
            ['email_verified_at' => null]
        ))->create();
    }
}
