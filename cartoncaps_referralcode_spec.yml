openapi: '3.0.3'
info:
  title: Carton Caps referral codes
  version: '1.0.0'
  description: |
    This is the API specification for Carton Caps describing the implementation of necessary endpoints to extend the existing API by the new referral code featureset.

    Authentication is necessary for all `ref` endpoints.
  contact:
    name: Adrian Missy
    email: adrian.missy@gmail.com
security:
  - bearerAuth: []
servers:
  - url: http://localhost/api/v1
tags:
  - name: Login
    description: |
      Authorization stand in for demonstration purposes. Real authorization would be handled by existing API and not be part of this spec.
  - name: ref
    description: Operations concerning referral codes
paths:
  /login:
    post:
      tags:
        - Login
      summary: Returns API bearer token.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                  example: demo@livefront.com
                password:
                  type: string
                  example: password
      responses:
        "200":
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  properties:
                    access_token:
                      type: string
        "401":
          $ref: "#/components/responses/UnauthorizedError"
  /ref:
    get:
      tags:
        - ref
      summary: Returns the currently authenticated user's referral link
      description: |
        It takes the user's referral code from their profile and uses a 3rd party vendor to create a deferred deep link which is then returned through this endpoint for the app to use in the sharing dialogue.
      responses:
        "200":
          description: successful operation
          content:
            application/json:
              schema:
                    type: string
                    description: |
                      The deferred deep link based on the referral code
                    example: https://deeplinks.lnk.click/my-deep-link
        "401":
          $ref: "#/components/responses/UnauthorizedError"
        "404":
          description: User was deleted and can not be found.
  /ref/list:
    get:
      tags:
      - ref
      summary: Lists all user's referrals and their statuses
      responses:
        "200":
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                      format: int64
                    name:
                      type: string
                    status:
                      type: string
                      description: |
                        Status of the referral:
                          - complete: referee is registered, email is confirmed
                          - pending: referee is registered, email has not been confirmed
                      enum:
                        - complete
                        - pending
              examples:
                All referrals completed:
                    description: >
                      All listed referrees have registered and confirmed their emails
                    value:
                      - id: 12
                        name: "Jenny S."
                        status: "complete"
                      - id: 64
                        name: "Archer K."
                        status: "complete"
                Some pending:
                    description: >
                      While user `Archer` has both registered and confirmed their email, `Helen` has not yet clicked the confirmation link to validate their email address and thereby not completed the referral.
                    value:
                      - id: 64
                        name: "Archer K."
                        status: "complete"
                      - id: 885
                        name: "Helen Y."
                        status: "pending"

        "401":
          $ref: "#/components/responses/UnauthorizedError"
        "404":
          description: Referral list is empty, no referrals have been made.

components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
  responses:
    UnauthorizedError:
      description: Access token is missing or invalid
