<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;
use Laravel\Sanctum\Sanctum;

class EdgeCases extends TestCase
{
    use RefreshDatabase;
    /**
     * What if a request for a referral code is made without proper authentication?
     *
     * @return void
     */
    public function test_user_is_not_logged_in()
    {
        $response = $this->get('/api/v1/ref', ["Accept"=>"application/json"]);
        $response->assertUnauthorized();
    }

    /**
     * What if the user was deleted (e.g. by admin because of abuse) and uses locally cached data to perform the request to generate more codes?
     *
     * @return void
     */
    public function test_user_does_not_exist_anymore()
    {
        $user = Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );
        User::destroy($user->id);
        $response = $this->get('/api/v1/ref', ["Accept"=>"application/json"]);
        $response->assertStatus(404);
    }

    /**
     * What if the referee uses a fake email which they can not confirm?
     *
     * @return void
     */
    public function test_referee_email_is_not_confirmed_so_referral_should_be_pending()
    {
        $user = Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );
        User::factory()->referred_by($user->referral_code)->unverified()->create();
        $response = $this->get('/api/v1/ref/list');
        $response->assertJsonFragment(['status' =>'pending']);
    }
}
