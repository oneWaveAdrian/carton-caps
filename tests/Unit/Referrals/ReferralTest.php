<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Illuminate\Database\Eloquent\Factories\Sequence;

class ReferralTest extends TestCase
{
    use RefreshDatabase;
    /**
     * How will the app generate referral links for the Share feature?
     *
     * @return void
     */
    public function test_app_can_generate_referral_links_for_share_feature()
    {
        $user = Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );
        $response = $this->get('/api/v1/ref');
        $response->assertContent("https://deeplinks.lnk.click/".base64_encode($user->referral_code));
    }

    /**
     * How will existing users check the status of their referrals?
     *
     * @return void
     */
    public function test_user_can_check_status_of_referrals()
    {
        $user = Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );
        $referrees = User::factory()->count(3)->referred_by($user->referral_code)->state(new Sequence(
            ['email_verified_at' => now()],
            ['email_verified_at' => null]
        ))->create();
        $assertionArray = [];
        foreach ($referrees as $ref) {
            array_push($assertionArray, ["id"=> $ref->id, "name"=> $ref->name, "status"=>!$ref->email_verified_at ? "pending" : "complete" ]);
        }
        $response = $this->get('/api/v1/ref/list');
        $response->assertJson($assertionArray);
    }

    /**
     * How will the app see if no referrals have been made by the user?
     *
     * @return void
     */
    public function test_user_has_not_made_any_referrals()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );
        $this->get('/api/v1/ref/list')->assertStatus(404);
    }
}
