<?php

namespace Tests\Feature;

use Tests\TestCase;

class BasicTest extends TestCase
{
    /**
     * Test if the application is running and reachable
     *
     * @return void
     */
    public function test_the_application_returns_a_successful_response()
    {
        $this->get('/api/v1/ping')->assertSee('pong!');
    }
}
