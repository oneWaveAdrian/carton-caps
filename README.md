# Carton Caps API extension

## Requirements
   - [Docker](https://www.docker.com)
   - make (optional)
   - [Composer](https://getcomposer.org)

## Usage
   Either use the Makefile commands provided or run their actions manually in your terminal as you prefer.
  
### Installation
      
   - `make install`: Installs Laravel setup and runs it
   
   If you are facing issues, see [Laravel Installation Guide](https://laravel.com/docs/9.x/sail#installing-composer-dependencies-for-existing-projects).

### Running
   - `make dev`: Runs the Laravel application locally in the background if it has previously been installed.
   - `make stop`: Stops the container.
   

### Testing
   Run `make test` in the terminal

### End of life
   Run `make uninstall` to remove the application and it's volume from your Docker instance

## API Spec
   The API spec file is called [cartoncaps_referralcode_spec.yml](https://gitlab.com/oneWaveAdrian/carton-caps/-/blob/main/cartoncaps_referralcode_spec.yml).
   It is kept in openAPI format for maximum compatibility and can be opened with your viewer of choice (e.g. [Insomnia](https://insomnia.rest/download), [Postman](https://www.postman.com), [SwaggerUI](https://swagger.io/tools/swagger-ui/)).

## Answers to questions
1. How will the app generate referral links for the Share feature?
   > The `/ref` endpoint returns the referral URL for the currently authenticated user
2. How will existing users create new referrals using their existing referral code?
   > By sharing the URL returned by the `/ref` endpoint through their medium of choice
3. How will the app know where to direct new users after they install the app via a referral?
   > The URL returned by the `/ref` endpoint is a deferred deep link which contains information for the app on which view to open. Inside the mock API, the returned URL has the style of a sample vendor, [kumulos](https://docs.kumulos.com/developer-guide/api-reference/deep-links/v1/#200-ok), as the task description contained a hint that a third party vendor will be used to add deferred deep link support to the app. My assumption is, that other vendors offer similarly styled deferred deep links.
4. How will existing users check the status of their referrals?
   > By using the `/ref/list` endpoint 
5. Since users may eventually earn rewards for referrals, should we take extra steps to mitigate abuse?
   >1. Abuse risk is deemed low as personal reward is low (community gain > personal gain) => counter measures can be low key as well
   >2. Reward redemption function should only run after new user confirmed their email address to prevent simple scripts with fake mail accounts
   >3. Reward redemption could be capped at a certain (realistically achievable) number of referrals per user
   >4. General mitigation measures should be in place already for whole API (e.g. rate limiter, user authentication, email confirmation,...) those will work for risk reduction on the rewards program as well

   ## License
      <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

      Adrian Missy