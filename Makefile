sail=./vendor/bin/sail

install:
	composer install
	cp .env.example .env
	${sail} up -d
	@echo ℹ️  Waiting for MYSQL container to start completely to prevent error on DB access
	sleep 10
	${sail} artisan key:generate
	${sail} artisan config:cache
	${sail} artisan migrate
	${sail} artisan db:seed

dev:
	${sail} up -d

test:
	clear
	${sail} test

stop:
	${sail} down

uninstall:
	${sail} down -v