<?php

use App\Http\Controllers\API\ReferralController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// System Health
Route::any('/ping', function () {
    return response('pong!');
});

// Authentication
Route::post('/login', [AuthController::class, 'login']);

// Referrals
Route::middleware('auth:sanctum')->group(function () {
    Route::get('/ref', [ReferralController::class,'getLink']);
    Route::get('/ref/list', [ReferralController::class,'getList']);
});
