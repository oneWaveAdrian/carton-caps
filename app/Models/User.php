<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'referer'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function referralLink()
    {
        //3rd party vendor deferred deep link generation logic would be inserted here instead
        return "https://deeplinks.lnk.click/".base64_encode($this->referral_code);
    }

    public function referees()
    {
        $referees = [];
        foreach (User::where('referer', $this->id)->get(["id", "name","email_verified_at"]) as $refs) {
            array_push($referees, ["id"=>$refs->id,"name"=>$refs->name,"status"=>!$refs->email_verified_at ? "pending" : "complete"]);
        }
        return $referees;
    }
}
