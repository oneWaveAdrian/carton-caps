<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ReferralController extends Controller
{
    public function getLink(Request $request)
    {
        $user = User::findOrFail($request->user()->id);
        return response($user->referralLink());
    }

    public function getList(Request $request)
    {
        $refs = $request->user()->referees();
        return sizeOf($refs) > 0 ? response(json_encode($refs)) : response('', 404);
    }
}
